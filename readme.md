I'm making this small repo just cuz. Here I'll store some TeX templates I have so I can have an small collection and also make proper documentation on how to use each of those.

As now I have nothing to show, and eventually I'll move all documentation to a webpage as I no longer plan to maintain the other wikis because of their lack of customization.

Just to put something useful, here is how to run OpTeX under TexWorks after installing the packages. All that is needed to do is to create the following Processing Tool under luatex.exe with the following parameters

```
-ftm
optex
$fullname
```