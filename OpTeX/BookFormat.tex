\fontfam[LMfonts]
\margins/1 a4 (3.5,2.6,3.5,3.5)cm
\typosize[11/13.6]
\parindent=17pt
\hyperlinks\Blue\Blue

\vglue0.5in

\begingroup
  \leftskip0pt plus 1fill
  \rightskip\leftskip
  \noindent
  \typosize[50/60]
  Group Theory
  \par
\endgroup

\vglue1in

\begingroup
  \leftskip0pt plus 1fill
  \rightskip\leftskip
  \noindent
  \typosize[40/50]
  J.S. Milne
  \par
\endgroup

\vglue2in

$$
\matrix{%
  &
  S_{3} &
  \cr
  r\hfill &
  = &
  \hfill%
  \pmatrix{%
    1 & 2 & 3 \cr
    2 & 3 & 1 \cr
  }
  \cr
  f\hfill &
  = &
  \hfill%
  \pmatrix{%
    1 & 2 & 3 \cr
    1 & 3 & 2 \cr
  }
}
$$

\begingroup
  \leftskip0pt plus 1fill
  \rightskip\leftskip
  \noindent
  \typosize[8/9]
  Version 4.00\nl
  June 23, 2021
  \par
\endgroup

\vfil\break

\begingroup
\parindent0pt

The first version of these notes was written for a first-year graduate algebra course. As in most such courses, the notes concentrated on abstract groups and, in particular, on finite groups. However, it is not as abstract groups that most mathematicians encounter groups, but rather as algebraic groups, topological groups, or Lie groups, and it is not just the groups themselves that are of interest, but also their linear representations. It is my intention (one day) to expand the notes to take account of this, and to produce a volume that, while still modest in size (c200 pages), will provide a more comprehensive introduction to group theory for beginning graduate students in mathematics, physics, and related fields.

\vfill
BibTeX information
\begtt
@misc{milneGT,
  author={Milne, James S.},
  title={Group Theory (v4.00)},
  year={2021},
  note={Available at www.jmilne.org/math/},
  pages={139}
}
\endtt

\vfill

Please send comments and corrections to me at jmilne at umich dot edu.

v2.01 (August 21, 1996). First version on the web; 57 pages.

v2.11 (August 29, 2003). Revised and expanded; numbering; unchanged; 85 pages.

v3.00 (September 1, 2007). Revised and expanded; 121 pages.

v3.16 (July 16, 2020). Revised and expanded; 137 pages.

v4.00 (June 23, 2021). Made work (including source code) available 
under Creative Commons licence.
\vfill

The multiplication table of $S_{3}$ on the front page was produced
by Group Explorer.
\vfill

Version 4.0 is published under a Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International licence 
(CC BY-NC-SA 4.0).
\bigskip

Licence information: 
\ulink[https://creativecommons.org/licenses/by-nc-sa/4.0/]{https://creativecommons.org/licenses/by-nc-sa/4.0}

Copyright \copyright 1996--2021 J.S. Milne.
\endgroup

\vfil\break

\secc Notation.

We use the standard (Bourbaki) notation: ${\bbchar N}=\{0,1,2,\ldots\}$; $\bbchar Z$ is the ring of integers; $\bbchar Q$ is the field of rational numbers; $\bbchar R$ is the field of real numbers; $\bbchar C$ is the field of complex numbers; ${\bbchar F}_{q}$ is a finite field with $q$ elements, where $q$ is a power of a prime number. In particular, ${\bbchar F}_{p}={\bbchar Z}/p{\bbchar Z}$ for $p$ a prime number.

For integers $m$ and $n$, $m|n$ means that $m$ divides $n$, i.e., $n\in m{\bbchar Z}$. Throughout the notes, $p$ is a prime number, i.e., $p=2,3,5,7,11,\ldots,1000000007,\ldots$.

Given an equivalence relation, $[*]$ denotes the equivalence class containing $*$. The empty set is denoted by $\emptyset$. The cardinality of a set $S$ is denoted by $|S|$ (so $|S|$ is the number of elements in $S$ when $S$ is finite). Let $I$ and $A$ be sets; a family of elements of $A$ indexed by $I$, denoted $(a_{i})_{i\in I}$, is a function $i\mapsto a_{i}: I → A$.\fnote{A family should be distinguished from a set. For example, if $f$ is the function ${\bbchar Z}→{\bbchar Z}/3{\bbchar Z}$ sending an integer to its equivalenceclass, then $\{f(i)\mid i\in{\bbchar Z}\}$ is a set with three elements whereas$(f(i))_{i\in{\bbchar Z}}$ is family with an infinite index set.}

Rings are required to have an identity element $1$, and homomorphisms of rings are required to take $1$ to $1$. An element $a$ of a ring is a unit if it has an inverse (element $b$ such that $ab=1=ba$). The identity element of a ring is required to act as $1$ on a module over the ring.

$$%
\matrix{%
  X\subset Y &
  X\mathbox{ is a subset of }Y\mathbox{ (not necessarily proper);} \cr
  X\buildrel\mathbox{def}\over{=}Y &
  X\mathbox{ is defined to be }Y\mathbox{, or equals }Y\mathbox{ by definition;} \cr
  X\approx Y &
  X\mathbox{ is isomorphic to }Y\mathbox{;}\cr
  X\simeq Y &
  X\mathbox{ and }Y\mathbox{ are canonically isomorphic (or there is a
given or unique isomorphism);}\cr
}
$$

\secc Prerequisites

An undergraduate “abstract algebra” course.

\secc Computer algebra programs

GAP is an open source computer algebra program, emphasizing computational group theory. To get started with GAP, I recommend going to Alexander Hulpke's page \ulink[http://www.math.colostate.edu/~hulpke/CGT/education.html]{here}, where you will find versions of GAP for both Windows and Macs and a guide “Abstract Algebra in GAP”. The Sage page \ulink[http://www.sagemath.org/]{here} provides a front end for GAP and other programs. I also recommend N. Carter's “Group Explorer” \ulink[https://nathancarter.github.io/group-explorer/index.html]{here} for exploring the structure of groups of small order. Earlier versions of these notes (v3.02) described how to use Maple for computations in group theory.

\secc Acknowledgements

I thank the following for providing corrections and comments for earlier versions of these notes: V.V. Acharya; Yunghyun Ahn; Max Black; Tony Bruguier; Vigen Chaltikyan; Dustin Clausen; Benoît Claudon; Keith Conrad; Demetres Christofides; Adam Glesser; Darij Grinberg; Sylvan Jacques; Martin Klazar; Thomas Lamm; Mark Meckes; Max Menzies; Victor Petrov; Flavio Poletti; Diego Silvera; Efthymios Sofos; Dave Simpson; David Speyer; Kenneth Tam; Robert Thompson; Bhupendra Nath Tiwari; Leandro Vendramin; Michiel Vermeulen.

Also, I have benefited from the posts to mathoverflow by Richard Borcherds, Robin Chapman, Steve Dalton, Leonid Positselski, Noah Snyder, Richard Stanley, Qiaochu Yuan, and others.

A reference {\bf monnnn} means question nnnn on mathoverflow.net and {\bf sxnnnn} similarly refers to \ulink[math.stackexchange.com]{math.stackexchange.com}.

\bye